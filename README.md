README for Alternate Variables Module

Description
===========
Alternate variables is designed to improve the website performance.
This module provides an alternate to core variable storage. 

Why we need alternate to core variable way of storing configuration: -

Excess use of variable_set and variable_get causes performance issues reason 
being variable_set makes the cache_bootstrap table clear with cid matching to 
'variables', its good to store configurations which are rarely updated, but any 
configuration which is subjected to update frequently will cause cache_bootstrap
table update, which in turn might be reason of slight performance degrade.

Also, all variables stored in variables table are loaded on each page load as 
they are part of global configurations, if any configuration is subjected to be 
used on each page load, variable_get is the best way to get it, However 
configurations which are contextual and not used globally get loaded 
unnecessarily which might cause memory issues if its used at large scale for all
configurations

This module basically provides:-
- Easy setter/getter/delete variable ability via api functions with a different 
storage table alternate_variable.
- Out of the box exportable configurations via features module 

Installation
============
  1. Place the entire alt_vars folder into your Drupal modules/ or better
     sites/x/modules/ directory.
  2. Enable the Alternate variables module by navigating to
     Administer > Modules

API exposed via this module
============================
- alt_vars_set($key,$value);
- alt_vars_get($key);
- alt_vars_delete($key);
